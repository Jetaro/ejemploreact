import React, { Component, useState } from 'react'; 
import {View,StyleSheet,TouchableOpacity,Text} from 'react-native';
import { RNCamera } from 'react-native-camera';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

class Ejemplo extends Component {
  constructor() {
    super();
    //this.state = {};

  }

  async componentDidMount() {
  
  }

  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.8, base64: true};//, path: "/../ejemplo2pics/"
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);

      this.props.navigation.navigate({name:'Probando',params:{foto:data}});
    }
  };

  render() {
    return (   
        <View style={styles.container}>
        <RNCamera
          style={styles.preview}
          ref={ref => {
            this.camera = ref
          }}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          androidCameraPermissionOptions={{
            title: 'Permiso para camara',
            message: 'Se necesitan permiso de camara',
            buttonPositive: 'Continuar',
            buttonNegative: 'Cancelar',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permiso para grabar audio',
            message: 'Se necesita permiso de microfono',
            buttonPositive: 'Continuar',
            buttonNegative: 'Cancelar',
          }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> Foto </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Ejemplo;