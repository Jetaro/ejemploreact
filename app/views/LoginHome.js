import React, {Component} from 'react';
import { Text, View, Image, Button, StyleSheet, StatusBar, Modal } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { TextInput } from 'react-native-gesture-handler';

const Stack = createNativeStackNavigator();

/*function _onPressFun(){
  //this.setModalVisibility(true);
  return (alert("Registro de informacion para guerreros."));
}*/

const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: StatusBar.currentHeight || 0,
    },
    item: {
      backgroundColor: '#f9c2ff',
      padding: 20,
      marginVertical: 8,
      marginHorizontal: 16,
    },
    title: {
      fontSize: 32,
    },
  });

  
const CatApp = () => {
  return (
    <View style={{ flex: 1, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-evenly' }}>
    </View>
  );
}


//export default CatApp;
export default class LoginHome extends Component {
  constructor(props) {
    super(props);

    //this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    
    this.state = {
      postext:"",
      postext2:"",
      estado: 'oli',
      visibility: false,
    };

    //this.Hint = React.createRef();
  }

  setModalVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  setPost1(value){
    this.setState({
      postext: value,
    })
  }

  setPost2(value){
    this.setState({
      postext2: value,
    })
  }

  _onPressFun(){
    this.setModalVisibility(true);
    alert("Registro de informacion para guerreros.");
  }

  postMsg(){
      const t1 = this.state.postext;
      const t2 = this.state.postext2;
      console.log(t1+t2);
      this.props.navigation.navigate({name:'Bienvenido',params:{text1: t1, text2: t2}});
  }

  render(){
    return (
      <View navigation={this.props.navigation} 
      style={{ flex: 1, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-evenly' }}>
        
        <Modal
          style={{ flex: 3, alignItems: 'center',alignContent: 'space-around' , justifyContent: 'space-evenly' }}
          animationType={'slide'}
          transparent={false}
          visible={this.state.visibility}
        >
          <View style={styles.modalContainer}>
            <View>
              <Text>Esta es la informacion de registro:</Text>
              <Text>-Mayor de 18 años.</Text>
              <Text>-No pertenecer a otros clanes.</Text>
              <Text>-En lo posible, ser un humano o gato!</Text>
              <Button
                color="#000"
                onPress={() => this.setModalVisibility(!this.state.visibility)}
                title="Cerrar información"
              />
            </View>
          </View>
        </Modal>
        
        <View >
        <Image
          source={require('../assets/img/login.png')}/*{uri: "https://reactnative.dev/docs/assets/p_cat1.png"}*/
          style={{width: 300, height: 200,  backgroundColor: 'skyblue'}}
        /></View>
        <View>
        <View>
        <Text>Nombre-clave</Text>
        <TextInput 
          style ={{height:"20%",borderColor: 'gray',
          borderWidth: 1}}
          Name="campo1"
          value={this.state.postext}
          onChangeText={(v)=>this.setPost1(v)}
          placeholder="Ingresar nombre-clave"/>
        <Text>Nombre-Real</Text>
        <TextInput 
          style ={{height:"20%",borderColor: 'gray',
          borderWidth: 1}}
          value={this.state.postext2}
          onChangeText={(v)=>this.setPost2(v)}
          Name="campo2"
          placeholder="Ingresar nombre-real"/></View>
        <View style={{ flexDirection: 'row',alignContent: 'space-around', justifyContent: 'space-evenly'}}>
        <View ><Button
            style ={{paddingHorizontal:8,
              paddingVertical: 6,
              borderRadius: 4,
              backgroundColor: "oldlace",
              alignSelf: "flex-start",
              marginHorizontal: "1%",
              marginBottom: 50,
              minWidth: "48%",
              textAlign: "center"}}
            title="Enviar Formulario"
            onPress={() => this.postMsg()} /></View>
        <View style={{ minWidth:10}}></View>
        <View ><Button
            style ={{paddingHorizontal: 8,
              paddingVertical: 6,
              borderRadius: 4,
              backgroundColor: "oldlace",
              alignSelf: "flex-start",
              marginHorizontal: "1%",
              marginBottom: 6,
              minWidth: "48%",
              textAlign: "center"}}
            title="Información"
            onPress={()=>this._onPressFun()}/></View></View>
        </View>
      </View>);
  }
}
