import React, { Component, useState } from 'react'; 
//import { useCallback, useEffect } from 'react';
import {
  Modal,
  Text,
  View,
  Button,
  StyleSheet,
  AppState,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import Geolocation from '@react-native-community/geolocation';
const API_KEY="sk.eyJ1IjoiamV0YXJvIiwiYSI6ImNsMHRqNjJtbzA0cGozZ29hOXhxbmJtcTAifQ.NpRMO9Rqi7k_JLT-uqHCIg";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  mainContainer: {
    marginTop: 22,
  },
  modalContainer: {
    marginTop: 22,
  },
});

class Ejemplo extends Component {
  constructor() {
    super();
    this.state = {
      visibility: false,
      cameraPermissionStatus:'not-determined',
      microphonePermissionStatus:'not-determined'
    };
    Geolocation.getCurrentPosition(info => {
      console.log(info)
      this.state = {
        geolocalizacion: info
      };
    })
//    useState<CameraPermissionStatus>('not-determined');
//    useState<CameraPermissionStatus>('not-determined');
  }

  async componentDidMount() {
    
    /*if (this.state.microphonePermissionStatus == 'not-determined'){
    console.log('Requesting microphone permission...');
    const permission1 = await Camera.requestMicrophonePermission();
    console.log(`Microphone permission status: ${permission1}`);
    
    if (permission1 === 'denied') await Linking.openSettings();
    //setMicrophonePermissionStatus(permission1);
    this.setState({
      visibility: false,
      microphonePermissionStatus:permission1
    });
    }

    if (this.state.cameraPermissionStatus == 'not-determined'){
    console.log('Requesting camera permission...');
    const permission2 = await Camera.requestCameraPermission();
    console.log(`Camera permission status: ${permission2}`);

    if (permission2 === 'denied') await Linking.openSettings();
    //setCameraPermissionStatus(permission2);
    this.setState({
      visibility: false,
      cameraPermissionStatus:permission2
    });
   }*/
  }

  setMicrophonePermissionStatus(perm){
    this.setState({
      visibility: false,
      microphonePermissionStatus:perm
    });
  }

  setCameraPermissionStatus(perm){
    this.setState({
      visibility: false,
      cameraPermissionStatus:perm
    });
  }

  setModalVisibility(visible) {
    this.setState({
      visibility: visible,
    });
  }

  cameraNavigate() {
    try{
      this.props.navigation.navigate({name:'Camera'});
    }
    catch(e){
      console.log(e);
      alert ("Se fue a la pulpi")};
  }

  getGeo(){
      Geolocation.getCurrentPosition(info => {
        this.setState({
          geolocalizacion: info
        });
      })
      console.log(this.state.geolocalizacion)

      return this.state.geolocalizacion
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <Modal
          animationType={'fade'}
          transparent={false}
          visible={this.state.visibility}
        >
          <View style={styles.modalContainer}>
            <View>
              <Text>Modal simple con geo</Text>
              <Text>{()=>this.getGeo()}</Text>
              <Button
                color="#000"
                onPress={() => {this.setModalVisibility(!this.state.visibility);this.getGeo();}}
                title="Cerrar"
              />
            </View>
          </View>
        </Modal>

        <Button
          color="#000"
          onPress={() => this.setModalVisibility(true)}
          title="Mostrar un modal"
        />
        <Button color="#000" onPress={() => this.cameraNavigate()} title="RNCamera" />
      </View>
    );//this.state.cameraPermissionStatus? :alert("No hay permisos.") 
  }
}

export default Ejemplo;