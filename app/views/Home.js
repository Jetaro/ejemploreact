import * as React from 'react';
import {Component,useState} from 'react';
import { View, Text, Button } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import DatePicker from 'react-native-date-picker';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      estado: 'oli',
      fecha: new Date(), 
    };
  }

  setFecha(date){
    this.setState({
      fecha: date,
    })
  }

  handleSubmit(e) {
    //e.preventDefault();
    console.log('You clicked submit 2.');
    this.props.navigation.navigate('Probando');
  }

  render(){
    try{
      console.log(this.props.navigation.getParams({text1}));
    }catch(e) {
      console.log(e);
    }

    return (
      <View 
      style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <View ><Button
              style ={{paddingHorizontal:8,
                paddingVertical: 6,
                borderRadius: 4,
                backgroundColor: "oldlace",
                alignSelf: "flex-start",
                marginHorizontal: "1%",
                marginBottom: 50,
                minWidth: "48%",
                textAlign: "center"}}
              title="Vamonos"
              onPress={()=>this.handleSubmit()} />{
              <DatePicker 
              fadeToColor={'orange'}
              mode={'date'}
              date={this.state.fecha} 
              onDateChange={(d)=>{this.setFecha(d)}} />/**/}
              <Text>{this.props.route.params?
                      (this.props.route.params.text1 != null? this.props.route.params.text1:""):""}</Text>      
        </View>
      </View>
    );
  }//this.state.fecha
}