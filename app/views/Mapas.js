import * as React from 'react';
import { Component } from 'react';
import { View, Text } from 'react-native';
import MapboxGL from "@react-native-mapbox-gl/maps";
import Geolocation from '@react-native-community/geolocation';

MapboxGL.setAccessToken("sk.eyJ1IjoiamV0YXJvIiwiYSI6ImNsMHRqNjJtbzA0cGozZ29hOXhxbmJtcTAifQ.NpRMO9Rqi7k_JLT-uqHCIg");

class Mapas extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      initialPosition: 'unknown',
      lastPosition: 'unknown',
      ini_x: 0,
      ini_y: 0,
    };

    Geolocation.getCurrentPosition(info => {
      console.log(info)
      this.state = {
        geolocalizacion: info
      };
    })
   }
    
    //watchID: ?number = null;
  
    componentDidMount() {
      Geolocation.getCurrentPosition(
        position => {
          const initialPosition = JSON.stringify(position);
          const ini_x = position.coords.latitude;
          const ini_y = position.coords.longitude;
          this.setState({initialPosition,ini_x,ini_y});
        },
        error => Alert.alert('Error', JSON.stringify(error)),
        {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
      );
      this.watchID = Geolocation.watchPosition(position => {
        const lastPosition = JSON.stringify(position);
        this.setState({lastPosition});
      });
      console.log(this.state.initialPosition)
    }
  
    componentWillUnmount() {
      console.log(this.state.initialPosition)
      //this.watchID != null && Geolocation.clearWatch(this.watchID);
    }

    renderAnnotations = () => {
      return (
        <MapboxGL.PointAnnotation
          key="pointAnnotation"
          id="pointAnnotation"
          coordinate={[this.state.ini_y, this.state.ini_x]}>
          <View style={{
                    height: 30, 
                    width: 30, 
                    backgroundColor: '#00cccc', 
                    borderRadius: 50, 
                    borderColor: '#fff', 
                    borderWidth: 3
                  }} />
        </MapboxGL.PointAnnotation>
      );
    }

  App() {
  return (
    <View style={{flex: 1, height: "100%", width: "100%" }}>
    <Text>Lon:{this.state.ini_y},Lat:{this.state.ini_x}</Text>
    <MapboxGL.MapView
      styleURL={MapboxGL.StyleURL.Street}
      zoomLevel={16}
      centerCoordinate={[this.state.ini_y, this.state.ini_x]}
      style={{flex: 1}}>
       <MapboxGL.Camera
          zoomLevel={16}
          centerCoordinate={[this.state.ini_y, this.state.ini_x]}
          animationMode={'flyTo'}
          animationDuration={0}
        >
      </MapboxGL.Camera>
      {this.renderAnnotations()}
    </MapboxGL.MapView>
  </View>
  );
}

    render(){
      return (this.App())
    };

}

export default Mapas;