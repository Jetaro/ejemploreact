/**
 * index de la aplicacion
 * desde aqui se cargara la APP
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './app/index.js';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
